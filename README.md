# How To Run/Test SauceDemo

* Import project to Katalon Studio
* Open Test Cases Folder
* Execute / Run per each of Test Cases: 
(Login, LoginLockoutuser, LoginPerformanceGlitchUser, LoginProblemUser)
* Pay Attention at the Log Viewer to check success/ failed test 


# How To Run/Test FakeRESTAPI

* Import project to Katalon Studio
* Open Test Cases Folder & Test Suite* Run/Execute all of test cases from Test Suite
* Pay Attention at the Log Viewer to check success/ failed test 

Postman Collection link: 
https://interstellar-water-990227.postman.co/workspace/fakerestAPI~de6bd84c-ecbe-4860-9583-b75cb4156764/collection/2356450-46e50fe2-992a-42e8-8d53-9255345f08a4?action=share&creator=2356450
